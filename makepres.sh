#! /bin/bash

# automate the conversion to reveal.js

Rscript -e 'library(knitr); knit("mem.Rmd",encoding="UTF-8")'
pandoc -t dzslides mem.md -o mem.html

cat head.html intro.html mem.html foot.html > index.html
perl -p -i'.bak' -e 's/class="slide level1"//' index.html

# vertical transforms -- very brittle
perl -p -i'.bak' -e 's/<p>::[[:blank:]]*vstart[[:blank:]]*::<\/p>/<section>/' index.html
perl -p -i'.bak' -e 's/<p>::[[:blank:]]*vnext[[:blank:]]*::<\/p>/<\/section>\n<section>/' index.html
perl -p -i'.bak' -e 's/<p>::[[:blank:]]*vend[[:blank:]]*::<\/p>/<\/section>/' index.html

# reduce header size
sed -i'.bak' 's/h2/h4/g' index.html
sed -i'.bak' 's/h1/h3/g' index.html
